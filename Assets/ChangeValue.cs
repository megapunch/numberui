﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeValue : MonoBehaviour {
	public static int maxGuessesAllowed = 10;
	public Text BrojPuta;
	public void Smanji() {
		maxGuessesAllowed = maxGuessesAllowed - 1;
		BrojPuta.text = maxGuessesAllowed.ToString();
	}

	public void Povecaj() {
		maxGuessesAllowed = maxGuessesAllowed + 1;
		BrojPuta.text = maxGuessesAllowed.ToString();
	}
}
