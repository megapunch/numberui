﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NumberWizards : MonoBehaviour {

    int max;
    int min;
    int guess;
	public Text text;
	public Text pokusaj;
    void Start() {
        StartGame();
	}

	void StartGame () {
        max = 1001;
        min = 1;
        guess = Random.Range(min, max + 1);
        pokusaj.text = "Ostalo je " + ChangeValue.maxGuessesAllowed.ToString() + " pokušaja";
    }

    public void GuessHigher() {
		min = guess;
		NextGuess();
	}

	public void GuessLower() {
		max = guess;
		NextGuess();
	}

    void NextGuess() {
		guess = Random.Range (min, max+1);
		text.text = guess.ToString() + "?";
        ChangeValue.maxGuessesAllowed = ChangeValue.maxGuessesAllowed - 1;
        pokusaj.text = "Ostalo je " + ChangeValue.maxGuessesAllowed.ToString() + " pokušaja";
        if (ChangeValue.maxGuessesAllowed <= 0) {
			SceneManager.LoadScene ("Win");
            ChangeValue.maxGuessesAllowed = 10;
		} 
    }
}
